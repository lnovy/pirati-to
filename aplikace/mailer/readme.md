
# Aplikace pro rozesílání mailů

## Opravnění a uživatelé

Autentizace: PirateID
Už. práva: vlastní (stačí editovatelné z DB)

Editoři budou cca 2 za kraj + max 10 celostátních.

Listy jsou 2 za kraj + celostátní

## Základní workflow 

1. Mail se napíše v markdownu
2. Náhled mailu v HTML (zobrazeném)
	1. Možnost zaslat testovací mail (každý list by měl mít své testovací maily?)
3. Výběr listů na které to chci poslat
	1. Listy se sloučí, odstraní se duplicity
	2. Odstraní se záznamy z blacklistu
4. Odeslání mailu, mail je uložen (dá se použít jako šablona pro příště)
5. V rámci mailu musí být možnost odhlásit se (ideálně s tím, že vyplní důvod), takový uživatel je zanesen do blacklistu
6. Statistiky z mailu?

## Data 

Je potřeba synchronizovat z DB fora alespoň 1 denně.

## Alternativy

* https://mailtrain.org

