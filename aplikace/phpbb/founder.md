Práva zakladatele PhpBB
=======================

Vedoucí administrativního odboru občas potřebuje nastavit práva zakladatele fóra. Zakladatel je nejvyšší oprávnění na fóru, které umožňuje dělat v nastavení fóra některé změny, které nemůže dělat ani administrátor. Z bezpečnostních důvodů neumožňujeme, aby měl některý uživatel práva zakladatele neustále.

Práva zakladatele se nastaví následujícím dotazem:
> update phpbb_users set user_type=3 where username='Martin.Kucera';

Na serveru je v crontabu následující řádek, který nastaví nejpozději po dvou hodinách sníží uživateli práva ze zakladatele na administrátora:

> 0 */2 * * * echo 'update phpbb_users set user_type = 0 where user_type = 3;' | /usr/bin/mysql pirati >/dev/null 2>&1

