
Domény jsou u [cloudflare][]. Většina domén je cloudflare přesměrována na server Sol (89.185.242.76)

Na Solu je další dělení řešeno v: `/etc/nginx/sites-available`. Kde jsou:

- redirecty: `10_redirects`
- gh pages: `20_gh_pirati-web`
- v dalších souborech jsou další subdomeny

Po změně je třeba:

1. otestovat nastavení: `nginx -t`
2. otočit nginx: `service nginx reload`

[cloudflare]: https://www.cloudflare.com/
