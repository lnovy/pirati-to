
# Vize

Vize, budoucí koncepce nebo lts todo.

## Mailer

Potřebujeme kvalitní mailer, který bude schopný snadno ovládat laik. Připadně dovolí i automatizované zasílání zpráv apod. Ideálně sám bude řešit zprávu mailinglistů, které vznikají někde živelně, nikoliv procesem složitější registrace - např. subscribe pro svobodnou hudbu, mailing list kraje (lidé, kteří nechtějí být na foru, ale chtějí vědět, co se děje).

Hlavní požadavky:

- Snadné psaní mailů
	- grafické (popř. markdown)
	- kvalitní UX
- Kvalitní správa skupin a to jak:
	- skupiny spravované v rámci aplikace (ad-hoc skupiny)
	- skupiny z fora (popř. z centrální DB identit)
	- v rámci ad-hoc skupin musí být snadná možnost odhlášení se
- Testovací maily
- Šablony
- Periodické maily
- Integrace s webem (přihlášování do ad-hoc mailinglistů)

Dobrou aplikací pro toto je [MailChimp][], avšak jeho licence a cena není přiznivá. Existuje opensource alternativa [MailTrain][].

Standa napsal ještě vlastní aplikaci v Nette, která posílá maily v markdownu, ale není v ní správa oprávnění apod.

## IdM

Správa identit je dneska řešena tak, že zdrojem identit je DB fóra. Z té čerpá GraphAPI a OpenID.

V realativně krátkém čase je třeba nasadit LDAP, abychom měli identity spárované (openID to nezaručuje). Též jsou s LDAPem IS obvykle lépe integrované (např. Redmine dovoluje přihlašování pouze skrze LDAP, ale u OpenID to neumí). [LdapJS][] by mohl být správnou cestou. Pokud bychom třeba chtěli nasadit [NextCloud][], tak by LDAP byl též potřeba pro kvóty apod.

Do budoucna chceme celou správu identit oddělit. Jako dobré, leč trochu hodně enterprise, řešení se jeví [midPoint][].


## Dohled

Dohledový systém pro správu vícero serverů, jejich statistiky apod.

Myslím si, že nejlepší OSS je [Zabbix][].


## Orchestrace

Orchestrace, čili automatizace, která nám dovoluje pracovat s více servery na jednou.

- [Puppet][]
- [Ansible][]: agentless


## Uložiště

Převážně odbory strany, zastupitelé a PKS pracují s docela velkým množstvím dokumentů (klasické office, obrázky, vektorová grafiky). Ty potřebují někde ukládat, sdílet, editovat. Zároveň je velmi nešťastné, pokud se "pohazují" po všech různých systémech strany (forum, wiki, Redmine, git), kde nejsou vůbec kontrolovány (nějaké rozumné kvóty, třídění apod). Bylo by výrazně lepší mít jedno dokumentové uložiště (nebo DMS), kde všechny dokumenty jsou, snadno je lze předávat. Popřípadě i promazávat.

- [NextCloud][]
- [Seafile][]


## Vizuální dokuemntace

Dokumentace je kámen úrazu skoro každé IT infrastruktury. Pokud se používá orchestrace a dohled, tak je mnoho věcí samozdokumentovaných. Nicméně ne vše. Např. vztahy mezi servery, doklady, záruky, přístupy... Jako zajímavá alternativa se mi jeví dokumentovat vše v aplikaci, která umí vzahy vizualizovat, vyhledávat apod. Takovou aplikací je např. [iTop][]. Ten je zároveň i servicedeskem.

[NextCloud]: http://nextcloud.org
[LdapJS]: http://ldapjs.org
[midPoint]: https://evolveum.com/midpoint/
[Zabbix]: http://www.zabbix.com
[Puppet]: https://puppet.com
[Ansible]: https://www.ansible.com
[Seafile]: https://www.seafile.com
[iTop]: http://www.combodo.com/itop-access-to-the-demonstration
[MailTrain]: http://mailtrain.org
[MailChimp]: https://mailchimp.com
