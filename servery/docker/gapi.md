# Graph API

```
git clone git@gitlab.com:pirati-cz/graph.git
cd graph
docker build -t gapi .
docker run --restart=always --name gapi -d -p 0.0.0.0:3042:3042 -e GAPI_DATABASE_USER=xxxx -e GAPI_DATABASE_PASSWORD=xxxx -e GAPI_DATABASE_NAME=pirati -e GAPI_DATABASE_HOST=xxxx gapi
```
