
# LDAP

Spuštění:

```
sudo docker run --name ldap --detach --env LDAP_ORGANISATION="Česká pirátská strana" --env LDAP_DOMAIN="pirati.cz" -p 389:389 osixia/openldap:1.1.6
```

Pro hesla jsou použity defaultní hodnoty.

Test:

```
sudo docker exec ldap ldapsearch -x -h localhost -b dc=pirati,dc=cz -D "cn=admin,dc=pirati,dc=cz" -w admin
```

Odpoveď:

```
# extended LDIF
#
# LDAPv3
# base <dc=pirati,dc=cz> with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# pirati.cz
dn: dc=pirati,dc=cz
objectClass: top
objectClass: dcObject
objectClass: organization
o:: w4TCjGVza8ODwqEgcGlyw4PCoXRza8ODwqEgc3RyYW5h
dc: pirati

# admin, pirati.cz
dn: cn=admin,dc=pirati,dc=cz
objectClass: simpleSecurityObject
objectClass: organizationalRole
cn: admin
description: LDAP administrator
userPassword:: e1NTSEF9S0RIcE5wY09aZzNwOHlWckVDQWMwMXkvMkpTOC91WnA=

# search result
search: 2
result: 0 Success

# numResponses: 3
# numEntries: 2
```

Luma: Auth: `simple`, bind as `cn=admin,dc=pirati,dc=cz`
