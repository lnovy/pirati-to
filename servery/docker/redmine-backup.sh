#!/usr/bin/env bash
# Author: Ondřej Profant
# Version: 1.0.0

app_host=hera
db_host=golem
db_name=redmine
remote_path=/var/lib/docker/data/redmine/
dir_name=rm-`date -I`

# Backup files
rsync -avzh --rsync-path="sudo rsync" --exclude "tmp" --exclude "dotfiles" --exclude "certs" "${app_host}:${remote_path}/" "$dir_name"
rsync -avzh -e "ssh" --rsync-path="sudo rsync" "${app_host}:${remote_path}/certs" "${dir_name}/certs"

# Backup DB
ssh ${db_host} "sudo mysqldump ${db_name} > ${dir_name}.sql"
scp ${db_host}:${dir_name}.sql .
ssh ${db_host} "rm ${dir_name}.sql"
