# Etherpad
```
docker build -t etherpad github.com/stanley89/etherpad-docker
docker run -d --name etherpad --restart=always -p 9001:9001 -e 'ETHERPAD_TITLE=Pirátský pad' -e 'MYSQL_ENV_DB_USER=xxxx' -e 'MYSQL_PORT_3306_TCP_ADDR=xxxx' -e 'MYSQL_ENV_DB_PASS=xxxx' -e 'MYSQL_ENV_DB_NAME=xxxx' etherpad
```
