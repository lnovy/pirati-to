# Redmine

|                 |                |
|-----------------|----------------|
| Server          | Hera           |
| Verze obrazu    | 3.3.0-4        | 
| Správce         | Ondřej Profant |
| Port https      | 8443           |
| Data na serveru | `/var/lib/docker/data/redmine` |

## Odkazy

* [Docker image](https://hub.docker.com/r/sameersbn/redmine/)
* [Image source codes](https://github.com/sameersbn/docker-redmine/)
* [Redmine homepage](http://www.redmine.org/)
* [Redmine plugins](http://www.redmine.org/plugins)

## Provoz

### Logy

Logy jsou v: `/var/cache/redmine/redmine`

Dostaneme se k nim např. pomocí:
```
docker run -it --rm --volumes-from=redmine busybox
```

### Perzistentní soubory

Krom DB, jsou ještě udržovány nějaké soubory na disku `/var/lib/docker/data/redmine

```
redmine/
├── certs
└── files
```

### Databáze

Je na [Golemu](../golem/readme.md).

## Záloha

Získání perzistentních dat z produkce:

```bash
./redmine.sh backup
```

Skript data uloží do struktury nad kterou rovnou můžete spustit lokální docker. Jen pozor na uživatelská práva.

### Obnova

Může nastat problém s SElinuxem, pak zkuste: `chcon -Rt svirt_sandbox_file_t <file> `

Soubory prostě umístíme na správné místo.

## Spuštění

Stačí doplnit heslo k DB a mailserveru a spustit:

```
sudo docker run -d --name redmine -p 8443:443 -e 'REDMINE_PORT=443' -e 'REDMINE_HTTPS=true' -e 'SMTP_HOST=mailgate.pirati.cz' -e 'SMTP_USER=redmine@pirati.cz' -e 'SMTP_PASS=' -e 'SMTP_STARTTLS=true' -e 'SMTP_TLS=false' -e 'SMTP_PORT=25' -e 'SMTP_OPENSSL_VERIFY_MODE=none' -e 'SMTP_AUTHENTICATION=:login' --env='DB_TYPE=mysql' --env='DB_ADAPTER=mysql2' --env='DB_HOST=192.168.122.6' --env='DB_NAME=redmine' --env='DB_USER=redmine' --env='DB_PASS=' --volume=/var/lib/docker/data/redmine:/home/redmine/data:Z sameersbn redmine:3.3.0-4
```



Konfigurace
-----------

Většina je jí zanesena přímo v parametrech kontejneru.

Pozor mám v instanci množství custom polí.

### CORS

[Skrz docker](https://github.com/sameersbn/docker-redmine/issues/147) se to nepodařilo, stejně tak není kompatbilní plugin.

Cors je povolen v `/etc/nginx/sites-enabled/31_redmine` na Solu.


### Pluginy

Viz https://redmine.pirati.cz/admin/plugins

#### Zajímavé pluginy 

kompatibilní s 3.*


|   Name          |      Desription                                                  |      Our note                              | Registrace | 
|-----------------|------------------------------------------------------------------|--------------------------------------------|------------|
| [Custom menu][]   | Allows to adjust the structure and items of Redmine menu.                      |           | Ano |
| [Work Time][]   | To view and update Spent time by each user.                      |           | Ne |
| [My page][]     | Providing some user specific customization in redmine.           | Dovoluje customizovat my page              | Ne |
| [Single auth][] | Single Sign On (SSO)                                             |           ||
| [dmsf][]        | Document Management System Features                              |           | |
| [mindmap][]     | Mindmap     || Ano|
| [2 factor auth][2fA] | SMS authentification |||

## Odkazy

http://vault-tec.info/post/68670739052/installing-migrating-upgrading-redmine-with
http://vault-tec.info/post/101096538356/redmine-on-centos-7


[Work Time]: http://www.redmine.org/plugins/redmine_work_time
[My page]: http://www.redmine.org/plugins/redmine_my_page
[Single auth]: http://www.redmine.org/plugins/single_auth
[dmsf]: https://github.com/danmunn/redmine_dmsf
[User mentions]: http://www.redmine.org/plugins/user_mentions
[mindmap]: http://www.redmine.org/plugins/mindmap-plugin
[2fA]: http://www.redmine.org/plugins/redmine_sms_auth
[Custom menu]: http://www.redmine.org/plugins/custom_menu
