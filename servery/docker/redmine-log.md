K logům se dostaneme pomocí: `docker run -it --rm --volumes-from=<rm_container> busybox`

Pro jednodušší práci si je můžeme vykopírovat: `docker cp <rm_container>:/var/log/redmine/redmine/production.log rm.log`

Logy používají klasickou rotaci. Dělí se na cron (např. pravidelné zasílání mailů), unicorn jsou stdout a stderr výpisy. Nejzajímavější je log `production`, 

```
ls /var/log/redmine/redmine/
cron_rake.log             cron_rake.log.4.gz        production.log.15.gz      unicorn.stderr.log.1      unicorn.stderr.log.5.gz
cron_rake.log.1           cron_rake.log.5.gz        production.log.16.gz      unicorn.stderr.log.10.gz  unicorn.stderr.log.6.gz
cron_rake.log.10.gz       cron_rake.log.6.gz        production.log.17.gz      unicorn.stderr.log.11.gz  unicorn.stderr.log.7.gz
cron_rake.log.11.gz       cron_rake.log.7.gz        production.log.18.gz      unicorn.stderr.log.12.gz  unicorn.stderr.log.8.gz
cron_rake.log.12.gz       cron_rake.log.8.gz        production.log.2.gz       unicorn.stderr.log.13.gz  unicorn.stderr.log.9.gz
cron_rake.log.13.gz       cron_rake.log.9.gz        production.log.3.gz       unicorn.stderr.log.14.gz  unicorn.stdout.log
cron_rake.log.14.gz       production.log            production.log.4.gz       unicorn.stderr.log.15.gz  unicorn.stdout.log.1
cron_rake.log.15.gz       production.log.1          production.log.5.gz       unicorn.stderr.log.16.gz  unicorn.stdout.log.2.gz
cron_rake.log.16.gz       production.log.10.gz      production.log.6.gz       unicorn.stderr.log.17.gz  unicorn.stdout.log.3.gz
cron_rake.log.17.gz       production.log.11.gz      production.log.7.gz       unicorn.stderr.log.18.gz  unicorn.stdout.log.4.gz
cron_rake.log.18.gz       production.log.12.gz      production.log.8.gz       unicorn.stderr.log.2.gz
cron_rake.log.2.gz        production.log.13.gz      production.log.9.gz       unicorn.stderr.log.3.gz
cron_rake.log.3.gz        production.log.14.gz      unicorn.stderr.log        unicorn.stderr.log.4.gz
```
Velikost jednoho gz je asi 9MB, bez komprese až stovky MB.

## Production log

Smazání úkolů číslo 4923, 5141, 5142, 5191:

```
cat production.log.*.gz | gunzip | grep -A 5 'Started DELETE "/issues/\(5141\|5191\|4923\|5442\)'
```


### Přihlášení uživatele

```
Successful authentication for 'admin' from 127.0.0.1 at 2017-02-28 10:38:23 UTC 
```

### Vytvoření úkolu

```
Started POST "/projects/projectname/issues" for 127.0.0.1 at 2017-02-28 10:53:36 +0000
Processing by IssuesController#create as HTML
  Parameters: {"utf8"=>"✓", "authenticity_token"=>"qfUCJiQqGHLE/3uQNbhH65Lb4o2Oq09utpclPsR1E0W5zcHpIVyua1Xd34EKBTAM1/FT09JBNZgWBhblTYaAOw==", "form_update_triggered_by"=>"", "issue"=>{"is_private"=>"0", "tracker_id"=>"3", "subject"=>"My first issue", "description"=>"My first issue", "status_id"=>"1", "priority_id"=>"2", "assigned_to_id"=>"1", "parent_issue_id"=>"", "start_date"=>"2017-02-28", "due_date"=>"", "estimated_hours"=>"", "done_ratio"=>"0"}, "was_default_status"=>"1", "commit"=>"Vytvořit", "project_id"=>"projectname"}
  Current user: admin (id=1)
  Rendered mailer/_issue.text.erb (6.8ms)
  Rendered mailer/issue_add.text.erb within layouts/mailer (8.5ms)
  Rendered mailer/_issue.html.erb (2.6ms)
  Rendered mailer/issue_add.html.erb within layouts/mailer (4.3ms)
Redirected to http://localhost:3300/issues/1
Completed 302 Found in 257ms (ActiveRecord: 24.1ms
```

### Zobrazení issue:

```
Started GET "/issues/1" for 127.0.0.1 at 2017-02-28 10:53:36 +0000
Processing by IssuesController#show as HTML
  Parameters: {"id"=>"1"}
  Current user: admin (id=1)
  Rendered issues/_action_menu.html.erb (6.9ms)
  Rendered issue_relations/_form.html.erb (3.3ms)
  Rendered issues/_relations.html.erb (9.3ms)
  Rendered issues/_action_menu.html.erb (2.2ms)
  Rendered issues/_form_custom_fields.html.erb (0.0ms)
  Rendered issues/_attributes.html.erb (7.3ms)
  Rendered issues/_form.html.erb (15.9ms)
  Rendered attachments/_form.html.erb (1.6ms)
  Rendered issues/_edit.html.erb (27.9ms)
  Rendered issues/_sidebar.html.erb (1.6ms)
  Rendered watchers/_watchers.html.erb (3.3ms)
  Rendered issues/show.html.erb within layouts/base (81.8ms)
Completed 200 OK in 166ms (Views: 84.6ms | ActiveRecord: 24.6ms)
```

### Smazání úkolu

```
Started DELETE "/issues/2" for 127.0.0.1 at 2017-02-28 11:04:11 +0000
Processing by IssuesController#destroy as HTML
  Parameters: {"authenticity_token"=>"VyI6f4SuefH9PPGmxQDUhOpJ7aMzGASSWA6vQwF416JHGvmwgdjP6GweVbf6vaNjr2Nc/W/yfmT4n5yYiItE3A==", "id"=>"2"}
  Current user: admin (id=1)
Redirected to http://localhost:3300/projects/projectname/issues
Completed 302 Found in 51ms (ActiveRecord: 19.3ms)
```

### Smazání z menu

```
Started DELETE "/issues?back_url=%2Fprojects%2Fprojectname%2Fissues&ids%5B%5D=3" for 127.0.0.1 at 2017-02-28 10:54:32 +0000
Processing by IssuesController#destroy as HTML
  Parameters: {"authenticity_token"=>"yU/ICIoF3wCrw+kAa0d7mUW6VccCOwV6Fdi+HZW+qGDZdwvHj3NpGTrhTRFU+gx+AJDkmV7Rf4y1SY3GHE07Hg==", "back_url"=>"/projects/projectname/issues", "ids"=>["3"]}
  Current user: admin (id=1)
Redirected to http://localhost:3300/projects/projectname/issues
Completed 302 Found in 44ms (ActiveRecord: 12.0ms)
Started GET "/projects/projectname/issues" for 127.0.0.1 at 2017-02-28 10:54:32 +0000
Processing by IssuesController#index as HTML
  Parameters: {"project_id"=>"projectname"}
  Current user: admin (id=1)
  Rendered queries/_filters.html.erb (6.0ms)
  Rendered queries/_columns.html.erb (2.4ms)
  Rendered issues/_list.html.erb (11.4ms)
  Rendered issues/_sidebar.html.erb (1.9ms)
  Rendered issues/index.html.erb within layouts/base (33.1ms)
Completed 200 OK in 83ms (Views: 43.3ms | ActiveRecord: 10.0ms)
```

