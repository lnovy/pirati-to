#!/usr/bin/env bash

x=1   # kolikaty pokus
db_img=mariadb:latest
app_img=sameersbn/redmine:3.3.2-1
original_data_dir=rm-`date -I`
data_dir=rm-`date -I`-$x
net=rednet-$x

echo "Clean"
sudo docker rm -f redmine-$x mariadb-$x
sudo docker network rm $net

echo -e "Vytvoření sítě, id: "
sudo docker network create $net

echo "Vytvoření DB, id:"
sudo docker run -d --name "mariadb-$x" --net $net --net-alias mariadb \
  -e MYSQL_ROOT_PASSWORD=redmine -e MYSQL_DATABASE=redmine -e MYSQL_USER=redmine -e MYSQL_PASSWORD=redmine \
  $db_img
sleep 9
sudo docker exec -i "mariadb-$x" \
  /bin/bash -c "export TERM=xterm && mysql -predmine -uredmine redmine" < "$original_data_dir.sql"

echo "Vytvoření APP"
cp -rT "$original_data_dir" "$data_dir"
mkdir -p "rm-logs"
sudo docker run -d --name redmine-$x --net $net --net-alias redmine -p 3000:80 \
  -e 'REDMINE_PORT=80' -e 'REDMINE_HTTPS=false' \
  --env='DB_TYPE=mysql' --env='DB_ADAPTER=mysql2' --env="DB_HOST=mariadb" --env='DB_NAME=redmine' --env='DB_USER=redmine' --env='DB_PASS=redmine' \
  --volume="$data_dir:/home/redmine/data:Z" \
  --volume="rm-logs:/var/log/redmine:Z" \
  $app_img
sleep 15
sudo docker logs redmine-$x

echo "Redmine běží na http://localhost:3000"
