
Neo4j
=====

Grafová databáze [neo4j][] využívaná projektem [goverMap][].

Využit je oficiální [docker image][image] v současnosti je v něm neo4j 2.3.*.


Instalace
---------

### Localhost

```bash
#!/usr/bin/env bash

dpath=/opt/docker/neo4j2/
pass=pirati23

mkdir -p ${dpath}/data
mkdir -p ${dpath}/conf
mkdir -p ${dpath}/ssl

docker pull docker.io/neo4j/neo4j:2.3.1
docker run \
  --detach \				# běh na pozadí / démon
  --publish=7474:7474 \			# standard http port
  --volume=${dpath}:/data \		# persistent volume
  --env=NEO4J_CACHE_MEMORY=512M \	# velikost cache
  --env=NEO4J_AUTH=neo4j/${pass} \	# nastaveni authentizace hesla
  docker.io/neo4j/neo4j:2.3.1
```


### Localhost

```bash
docker pull neo4j/neo4j
mkdir -p /opt/docker/neo4j/data
mkdir -p /opt/docker/neo4j/conf

# Test (with terminal):
sudo docker run -i -t --rm \
 --name neo4j \
 -v /opt/docker/neo4j/data:/data \
 -v /opt/docker/neo4j/conf:/conf \
 -p 8476:7474 \
 -p 8475:7473 \
 neo4j/neo4j

# Daemon:
sudo docker run -d \
 --name neo4j \
 --restart=always \
 -v /opt/docker/neo4j/data:/data \
 -v /opt/docker/neo4j/conf:/conf \
 -p 8476:7474 \
 -p 8475:7473 \
 neo4j/neo4j
```

Data jsou tedy uložena v `/opt/docker/neo4j`

### Server

```
sudo mkdir -p /var/lib/docker/neo4j/data
docker run -d --name neo4j -p 7474:7474 -v /var/lib/docker/neo4j/data:/data docker.io/neo4j/neo4j
```

Uživatelé
---------

Bohužel neo4j nemá příliš sofistikovanou správu uživatelských účtů.
Ve verzi 2.2 si vynutí jeden účet při prvním spuštění a tím to končí.
Další uživatele [lze přidat][auth], ale neexistuje k tomu pohodlná cesta.
Tyto neduhy by měla napravit verze 2.3.


Vice DB
-------

Neo4j nemá rozdělení na databáze jako známe z jiných DB.
Každá instance je právě jedna databáze - což nám samozřejmě skrz docker nečiní problémy.
Další DB rozjedeme na dalším portu.



neo4j: http://neo4j.com
auth: https://leanjavaengineering.wordpress.com/2015/04/16/neo4j-2-2-authentication-and-adding-extra-users/
image: https://registry.hub.docker.com/u/neo4j/neo4j/
goverMap: https://github.com/pirati-cz/goverMap
