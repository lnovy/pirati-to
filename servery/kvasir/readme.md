Popis konfigurace virtualniho serveru
=====================================
kvasir
======

*Ucel*: mailserver
*Typ*: virtual;HVM - KVM
*Domovske umisteni*: kvmpirhost1-master

Zakladni informace
------------------

Hostname:
IP: 89.185.242.77/32
Brana: jako kvmpirhost1
DNS: jako kvmpirhost1
CPU: 2
Pamet: 1GiB
OS: Debian Jessie
Disky: 1x30GiB (0QEMU_QEMU_HARDDISK)

Nastaveni
---------
* disky
  * rozdeleni 0QEMU_QEMU_HARDDISK(position 0)
	- sda1 200MiB
	label=kvasir_boot
	- sda2 29.8GiB
  - LVM kvasir_vgroot2
	- kvasir_lvroot 24GiB /root ext4
	label=kvasir_root
	- kvasir_lvvarlog 3GiB /var/log ext4
	label=kvasir_varlog
	- kvasir_lvswap 1GiB

Firewall
--------
* umisteni /etc/init.d/firewall.sh
  - ovladani "systemctl [stop|start|restart] firewall.sh"
* firewall mimo jine i restartuje fail2ban-clienta
* otevrene porty
  - ssh:22
  - smtp:25
  - smtps:587
  - ping

Sluzby
------
Posta je rozesilana pomoci postfix a to z lokalnich souboru,
alias soubor je generovan z mysql databaze(db fora na golem).
Prichozi a odchozi posta je kontrolovana proti SPAMu(blklist,
hostname a HELO) i proti virum a pridava DKIM. Aktualne se na serveru
nehostuji zadne schranky, pouze se preposilaji. Uzivatele jsou
definovani lokalne v souboru.

* rozesilani/prijimani posty
  * postfix
    - vynucene sifrovani naportu 587, ktery slouzi zejmena pro
	  posilani posty po prihlaseni
	- soubory postfixu v adresari /etc/postfix/virtuals
	  + adresar domain_files "je priprava" pro domeny, ktere maji
	    lokalni schranky **z tohoto souboru se generuje soubor
	    virtualmailboxes(fyzicke lokalni schranky)
	  + adresar maps obsahuje mapy domena, uzivatelu, transportu,
	    ssl vyjimek, aliasu[*], hlavicek, atd.[2*]
    - po uprave je **nutne vygenerovat databazi**
      script v adresari "!run_postmap.sh" to udela za vas
	- generovaci skript aliasu Piratu v cronu se pousti 1x hodinu z
	  /etc/cron.d/mkcpsaliases a uklada do souboru
	  /etc/postfix/virtuals/maps/virtualaliases_piraticz
  * amavis
	- napojen na postfix, stara se o spam/AV kontrolu, DKIM
	  + virus clamav
	  + spam spamassasin
	- soubory amavisu jsou v adresari /etc/amavis/conf.d
      + upravy se provadi v souboru 50_user
	  + DKIM soubory /var/lib/dkim/
	  + karantena /var/lib/amavis/virusmails **obcas nutno promazat**
	- soubory clamav jsou v /etc/clamav/
	- soubory spamassasin jsou v /etc/spamassasin/
	  + upravy v souboru local.cf
  * dovecot
	- zatim neaktivni(imap-pop3 server)
	- soubory v /etc/dovecot
	- pripraveny adresar pro lokalne ulozene maily /home/vmails



[*] pro generovani aliasu se pouziva script mkcpsaliases*[1]
[2*] aktualne se aktivne pouziva pouze virtualdomainaliases
obsahujici virtualni domeny a soubor aliases obsahujici vygenerovane
aliasy*

[1]: https://gitlab.com/pirati-cz/mkcpsaliases*
