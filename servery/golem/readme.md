
# Golem

Zde běží databáze (MariaDB, PostgreSQL), server je přístupný pouze z `kvmpirhost`. OS je Debian 8 Jessie.

## Přístup

V `./ssh/config` je třeba si nastavit:

```
# Piráti: DB stroj
Host golem
HostName 192.168.122.6
ProxyCommand ssh kvmpirhost nc %h %p
```

## MariaDB

- jak založit DB a příslušné uživatele (včetně bezpečnostních zásad)

Běží tam:

| Databáze       | Komentář               |
|----------------|------------------------|
| ao             | |
| etherpad       | std. mysql etherpad DB |
| mailer         | |
| mailer2\_old   | |
| mailer\_old    | |
| ovk            | |
| phpbb\_beta    | |
| pirati         | |
| piwik          | |
| piznam         | |
| praha          | ručně plněná DB        |
| redmine        | std. mysql RM db       |
+----------------|------------------------|


## PostgreSQL

- jak založit DB a příslušné uživatele (včetně bezpečnostních zásad)

## Zálohování

- [lokální](backup.md)
- [odsunutí](wedos.md)

