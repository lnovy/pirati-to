Zálohování db na golemu
=======================

* postgresql a mariadb se automaticky lokálně zálohují pomocí automysqlbackup a autopostgresqlbackup
* nejnovější záloha je v /var/lib/automysqlbackup/latest resp. /var/lib/autopostgresqlbackup/latest
* konfigurace je v /etc/default/automysqlbackup a /etc/default/autopostgresqlbackup
* po provedení zálohy se spustí skript v /etc/mysql-post-backup resp. /etc/postgresql-post-backup
* zálohy z výše uvedených adresářů jsou zašifrovány pomocí gpg a uloženy na wedos disk
* wedos disk je připojen přes sambu a stunnel - viz wedos.md
