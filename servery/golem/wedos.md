Připojení wedos disku přes cifs a stunnel
=========================================

> apt-get install stunnel4 cifs-utils

v /etc/default/stunnel4 změnit ENABLED=1

do /etc/stunnel/samba.conf:
> client = yes
>
> pid=/var/run/stunnel-samba.pid
>
>
> [samba445]
>
> accept = 445
>
> connect = 13407.s7.wedos.net:446
>
>
> [samba139]
>
> accept = 139
>
> connect = 13407.s7.wedos.net:140

restartovat stunnel:
> service stunnel4 restart

připojit wedos disk:
> mount //localhost/UZIVATEL /mnt/wedos -o username=UZIVATEL -t cifs -v

Záznam do /etc/fstab:
> //localhost/UZIVATEL /mnt/wedos cifs	credentials=/root/.smbcredentials 0 0

Obsah /root/.smbcredentials:
> username=UZIVATEL
>
> password=HESLO

