Popis konfigurace serveru
=========================

Zakladni info
-------------
hostname = kvmpirhost2-spoje.pirati.cz
IP = 77.87.240.89
umisteni = spoje.net "**RACK??**"
OS = Debian Stretch

disky = 2x 1TiB (Toshiba MG03ACA100)
SN =	86BEKQTOF
SN =	86BDK0LDF


Nastaveni
---------
* disky
  * rozdeleni MG03ACA100-86BEKQTOF
    - sda1 500MiB bootable
    - sda2 84GiB
    - sda3 847GiB
  * rozdeleni MG03ACA100-86BDK0LDF
    - sdb1 500MiB bootable
    - sdb2 84GiB
    - sdb3 847GiB
  * RAID 1 - sda,sdb(2x MG03ACA100)
    - md0 500MiB /boot label=boot-kvmpirhost2 ext2
    - md1 84GiB lvm rootvg-kvmpirhost2-spoje
    - md2 857GiB lvm datavg-kvmpirhost2-spoje
  * LVM rootvg-kvmpirhost2-spoje
    - rootlvkvmpirhost2-spoje 40GiB /root ext4
	label=root-kvmpirhost2-spoje
    - varlvkvmpirhost2-spoje 10GiB /var ext4
	label=var-kvmpirhost2-spoje
    - varloglvkvmpirhost2-spoje 6GiB /var/log ext4
	label=varlog-kvmpirhost2-spoje
    - tmplvkvmpirhost2-spoje 10GiB /var/log ext4
	label=tmp-kvmpirhost2-spoje
    - swaplvkvmpirhost2-spoje 8GiB /var/log swap
  * LVM datavg-kvmpirhost2-spoje
	- encdatalvkvmpirhost2-spoje 300GiB (DM-crypt)
      - encdatalv /mnt/enc_data ext4
	    label=encdata-kvmpirhost2-spoje
	    mkfs.ext4 -T largefile4 -L encdata-kvmpirhost2-spoje
	- plaindatalvkvmpirhost2-spoje 200GiB /mnt/plain_data ext4
	  label=plaindata-kvmpirhost2-spoje
	  mkfs.ext4 -T largefile4 -L plaindata-kvmpirhost2-spoje

Firewall
--------
* umisteni /etc/init.d/firewall.sh
* ovladani systemctl [stop|start|restart] firewall.sh
* firewall mimo jine i restartuje fail2ban-clienta a
  libvirt kvuli default network iptables pravidlum **TODO najit lepsi cestu**

Nataveni ip
-----------
* interfaces 
  - br0 - most(drzitel IPcek)
    + eno1 - otrok
    + eno2 - otrok (BACKUP-neni zapojeny kabel)
* IP: 77.87.240.89
  **aktualne NATovano na defaultni IP kvmpirhost2-spoje**


Nastaveni sifrovani
-------------------
* volani sluzby "systemctl start mnt-enc_data.mount"
* /etc/crypttab
  encdatalv UUID=b8fa80d4-a8f2-49de-90f7-18a49f9e2f8f none luks,noauto 
* parametry sifrovani


cryptsetup -v --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 5000 --use-random luksFormat /dev/datavg-kvmpirhost2-spoje/encdatalvkvmpirhost2-spoje

se as:             physical volume for encryption                         │   
  │                      Encryption method:  Device-mapper (dm-crypt)                               │   
  │                                                                                                 │   
  │                      Encryption:         aes                                                    │   
  │                      Key size:           512                                                    │   
  │                      IV algorithm:       xts-plain64                                            │   
  │                      Encryption key:     Passphrase                                             │   
  │                      Erase data:         yes 

libvirt-clients

Security
--------
apparmor
iptables
fail2ban
logcheck
auditd

Uzivatele
---------
root = ZAKAZAN

* sudoers
  - djz88:ZK:plny root
  - stanley:SŠ:plny root
  - keddie:OP:plny root
  - **Genericky piratsky ucet admin - heslo na flash/papiru**
   **a hlavicka zaheslovaneho oddilu??**
* bez prav

Sluzby
------
openssh daemon
kvm

Vzdaleny pristup
----------------
openssh
qemu over ssh



Hesla
-----
Kryptovani disku = **VLozit heslo???**

*OTAZKY*
========
Zaheslovat bios?
Zaheslovat grub?
Encryptovat root?
Povolit root? -> nepovolen bez hesla maintenance mod
Nastavit monitorovani bezicich sluzeb(pripadne restart)?
Nastavit audit uzivatelu?
Jak vyresit zalohy?

Balicky
-------
smartmontools
cryptsetup
postfix
vim
libvirt-daemon
libvirt-daemon-system
qemu-block-extra
sgabios
logcheck
syslog-summary
screen
rngd-tools

TODO
====
nastavit smartmontools
nastavit sensory
nastavt firewall-systemd
nastavit monitorovani disku



Libvirt
-------
apt-get install libvirt-daemon libvirt-daemon-system qemu-block-extra sgabios

Apparmor
--------
apt-get install apparmor apparmor-profiles apparmor-utils
perl -pi -e 's,GRUB_CMDLINE_LINUX="(.*)"$,GRUB_CMDLINE_LINUX="$1 apparmor=1 security=apparmor",' /etc/default/grub
update-grub

x*

Postfix
-------
nasloucha na localhostu a odesila maily rootovi

Rsyslog
-------
*.*;auth,authpriv,cron,mail.none                -/var/log/syslog
cron.*                          /var/log/cron.log

x*

Smartd
------
v /etc/default/smartd povolen daemon smartd
start_smartd=yes

v /etc/smard.conf nastaveno monitorovani disku (errorlog a teplota - do logu stoupne-li o 4stupne a kriticke hodnoty 45,55)
/dev/sda -a -l error -t -I 194 -W 4,45,55 -o on -S on -s (S/../.././02|L/../../6/03)
/dev/sdb -a -l error -t -I 194 -W 4,45,55 -o on -S on -s (S/../.././02|L/../../6/03)

-Polkit
-------
cat /etc/polkit-1/localauthority/50-local.d/50-org.libvirt-group-access.pkla

-[libvirt group Management Access]
-Identity=unix-group:sudo
-Action=org.libvirt.unix.manage
-ResultAny=yes
-ResultInactive=yes
-ResultActive=yes
