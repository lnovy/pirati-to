Popis konfigurace serveru
=========================

Zakladni info
-------------
hostname = kvmpirhost1-master.pirati.cz  
IP serveru = 89.185.242.75
- Dalsi IP1 = 89.185.242.76
- Dalsi IP2 = 89.185.242.77
- Dalsi IP3 = 89.185.242.78
Brana: 89.185.242.1  
DNS: 81.31.33.19  
	 80.79.16.5  
umisteni = master "**RACK???**"  
OS = Debian Jessie

disky = 2x 1TiB (ST1000NM0033-9ZM173)
SN =	Z1W4E0E6
SN =	Z1W4DWBE


Nastaveni
---------
* disky
 - rozdeleni ST1000NM0033-9ZM173-Z1W4E0E6
   - sda1 500MiB bootable
   - sda2 93GiB
   - sda3 838GiB
 * rozdeleni ST1000NM0033-9ZM173-Z1W4DWBE
   - sdb1 500MiB bootable
   - sdb2 93GiB
   - sdb3 838GiB
 * RAID 1 - sda,sdb(2x ST1000NM0033-9ZM)
   - md0 500MiB /boot label=boot-kvmpirhost1-master ext2
   - md1 93GiB lvm rootvg-kvmpirhost1-master
   - md2 838GiB crypt datavg-kvmpirhost1-master
 * LVM rootvg-kvmpirhost1-master
   - rootlvkvmpirhost1-master 40GiB /root ext4
   label=root-kvmpirhost1-master
   - varlvkvmpirhost1-master 10GiB /var ext4
   label=var-kvmpirhost1-master
   - varloglvkvmpirhost1-master 6GiB /var/log ext4
   label=varlog-kvmpirhost1-master
   - tmplvkvmpirhost1-master 10GiB /var/log ext4
   label=tmp-kvmpirhost1-master
   - swaplvkvmpirhost1-master 8GiB /var/log swap
 * LVM datavg-kvmpirhost1-master
   - encdatalvkvmpirhost1-master 300GiB (DM-crypt)
     - encdatalv /mnt/enc_data ext4
	   label=encdatalv-kvmpirhost1-master
	   mkfs.ext4 -T largefile4 -L encdata-kvmpirhost1-master
   - plaindatalvkvmpirhost1-master 200GiB /mnt/plain_data ext4
	 label=plaindata-kvmpirhost1-master
	 mkfs.ext4 -T largefile4 -L plaindata-kvmpirhost1-master

Firewall
--------
* umisteni /etc/init.d/firewall.sh
* **TODO KONVERTOVAT do nativniho systemctl**
  **TODO ovladani systemctl [stop|start|restart] firewall.sh**
  - ovladani service firewall.sh [stop|start|reload]
* firewall mimo jine i restartuje fail2ban-clienta a
  libvirt kvuli default network iptables pravidlum **TODO najit lepsi cestu**

Nastaveni ip
------------
* interfaces
  - br0 - most(drzitel IPcek)
  - eth0 otrok
 ** + eth1 - potreba dat do mostu jako backup **
  

* IP: 89.185.242.76
  *NAT* - IP na kvmpirhost1-master NIC
 - slouzi pro aplikace
   - ssh na virtualy
   - mysql:golem
   - web forum:hnoss
   - web rozcestnik:nginx
   - doker:
	 - redmine
   - systemd-nspawn:vali
     - mumble
     - **ejabberd??**
     - **mailer??**
* IP: 89.185.242.77
  *DEDIKOVANA* IP pro
   - mailserver:kvasir
* IP: 89.185.242.78
  *NAT* - IP na kvmpirhost1-master NIC
 - slouzi pro aplikace
   - web rozcestnik:nginx

Nastaveni sifrovani
-------------------
* volani sluzby "systemctl start mnt-enc_data.mount"
* /etc/crypttab:
  encdatalv UUID=3eae3c9e-d05b-45fb-8afc-79883c942f5b none luks,noauto
* parametry sifrovani  

cryptsetup -v --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 5000 --use-random luksFormat /dev/datavg-kvmpirhost1-master/encdatalvkvmpirhost1-master


se as:             physical volume for encryption                         │   
  │                      Encryption method:  Device-mapper (dm-crypt)                               │   
  │                                                                                                 │   
  │                      Encryption:         aes                                                    │   
  │                      Key size:           512                                                    │   
  │                      IV algorithm:       xts-plain64                                            │   
  │                      Encryption key:     Passphrase                                             │   
  │                      Erase data:         yes 

Security
--------
apparmor  
iptables  
fail2ban  
logcheck  
**??auditd??**  

Uzivatele
---------
root = ZAKAZAN

* sudoers
  - djz88:ZK:plny root
  - stanley:SŠ:plny root
  - keddie:OP:plny root
  - **Genericky piratsky ucet admin - heslo na flash/papiru
	a hlavicka zaheslovaneho oddilu??**

* bez prav

Sluzby
------
openssh daemon  
kvm  

Vzdaleny pristup
----------------
openssh  
qemu over ssh  

Hesla
-----
Kryptovani disku = **VLozit heslo???**

*OTAZKY*
========
Zaheslovat bios?  
Zaheslovat grub?  
Encryptovat root?  
Povolit root? -> nepovolen bez hesla maintenance mod  
Nastavit monitorovani bezicich sluzeb(pripadne restart)?  
Nastavit audit uzivatelu?  
Jak vyresit zalohy?  

Balicky
-------
smartmontools
cryptsetup
postfix
vim
libvirt-daemon
libvirt-daemon-system
qemu-block-extra
sgabios
logcheck
syslog-summary
screen
rngd-tools

TODO
====
nastavit smartmontools  
nastavit sensory  
nastavt firewall-systemd  
nastavit monitorovani disku  



Libvirt
-------
apt-get install libvirt-daemon libvirt-daemon-system qemu-block-extra sgabios

Apparmor
--------
apt-get install apparmor apparmor-profiles apparmor-utils
perl -pi -e 's,GRUB_CMDLINE_LINUX="(.*)"$,GRUB_CMDLINE_LINUX="$1 apparmor=1 security=apparmor",' /etc/default/grub
update-grub

x*

Postfix
-------
nasloucha na localhostu a odesila maily rootovi

Rsyslog
-------
*.*;auth,authpriv,cron,mail.none                -/var/log/syslog
cron.*                          /var/log/cron.log

x*

Smartd
------
v /etc/default/smartd povolen daemon smartd
start_smartd=yes

v /etc/smard.conf nastaveno monitorovani disku (errorlog a teplota - do logu stoupne-li o 4stupne a kriticke hodnoty 45,55)
/dev/sda -a -l error -t -I 194 -W 4,45,55 -o on -S on -s (S/../.././02|L/../../6/03)
/dev/sdb -a -l error -t -I 194 -W 4,45,55 -o on -S on -s (S/../.././02|L/../../6/03)

Polkit
-------
cat /etc/polkit-1/localauthority/50-local.d/50-org.libvirt-group-access.pkla

-[libvirt group Management Access]
-Identity=unix-group:sudo
-Action=org.libvirt.unix.manage
-ResultAny=yes
-ResultInactive=yes
-ResultActive=yes
