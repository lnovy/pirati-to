kvmpirthost2-spoje
==================
copied from kvmpirhost1


config changes
--------------
- copy /root/.profile
- copy /root/.bashrc

- set vim defaults at /etc/vim/vimrc
  + syntax on; set backgroud=dark; 
  + let g:skip_defaults_vim = 1
  + uncomment set showcmd;set showmatch;set ignorecase
  + uncomment jump to the last position section

- /etc/bash.bashrc
  + export HISTSIZE=10000
  + uncomment bash completion section

- /etc/crypttab
  + add
    encdatalv UUID=b8fa80d4-a8f2-49de-90f7-18a49f9e2f8f none luks,noauto

- /etc/fstab
  + add plain + enc data (enc_data are from encdatalv which will be created from crypttab)
    /dev/mapper/datavg--kvmpirhost2--spoje-plaindatalvkvmpirhost2--spoje /mnt/plain_data ext4 defaults 0 2
    /dev/mapper/encdatalv /mnt/enc_data   ext4    defaults,noauto        0       2
 
- /etc/polkit-1/localauthority/50-local.d/50-org.libvirt-group-access.pkla
  + create libvirt file to be able to use virt-manager with users 
[libvirt group Management Access]
Identity=unix-group:sudo
Action=org.libvirt.unix.manage
ResultAny=yes
ResultInactive=yes
ResultActive=yes


Filesystems
-----------
created fs for pain_data and enc_data



package install
--------------
- man
- bash-completion
- cryptsetup
- cryptsetup-bin
- nmap
- mc
- fail2ban
- libvirt-daemon
- libvirt-clients
- apt-listchanges
- apt-listbugs
- libpolkit-agent-1-0
- libpolkit-backend-1-0
