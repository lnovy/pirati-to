Kolekce svobodne hudby byla presunuta z Lori na Hermes kvuli nedostatku mista na Lori a chystane reinstalaci Lori. Za ucelem jejiho provozovani bezi na Hermesu dva docker kontejnery - jeden pro pristup pres sftp a druhy pro web.

docker run --restart=always --name hudba-sftp -v /var/lib/docker/data/hudba:/home/hudba/ -p 2201:22 -d atmoz/sftp jmeno:heslo:uid(500 pro core)

docker run --name nginx-hudba --restart=always -v /var/lib/docker/data/hudba/www:/usr/share/nginx/html:ro -v /var/lib/docker/data/nginx/:/etc/nginx/conf.d -p 8001:80 -d nginx
