
Server Orion
============

Dedikovaný server u Server4you. Cena 89.99 USD za měsíc + 1 USD/ipv4 za měsíc.

IP adresy:

* 85.25.44.113
* 85.25.203.229
* 62.75.254.193

Běží na něm
-----------

* Hermes
	* [Redmine](redmine.md)
	* [Neo4j](neo4j.md)
	* [LorriDbSlave](lorridbslave.md)
