Manual
======

On SLAVE machine:

```
git clone https://github.com/lnovy/loridbslave-docker
cd loridbslave-docker
docker build -t loridbslave .
docker run -d --name loridbslave -v /var/lib/docker/data/loridbslave:/var/lib/mysql loridbslave
```

On MASTER mysql:

```
grant all privileges on *.* to 'root'@'orion1603.startdedicated.com' identified by 'xxxx';
GRANT REPLICATION SLAVE ON *.* TO 'slave_user'@'orion1603.startdedicated.com' IDENTIFIED BY 'xxx';
flush privileges;
FLUSH TABLES WITH READ LOCK;
show master status;
```

ON SLAVE machine:

```
docker exec -ti loridbslave /bin/bash
mysqldump -u root -h pirati.cz --ssl-ca=/etc/mysql/ca-cert.pem -p --databases pirati piznam petice_konopi |mysql
```

ON MASTER mysql:

```
unlock tables;
```

On SLAVE mysql:

```
CHANGE MASTER TO MASTER_HOST='pirati.cz',MASTER_PORT=3306, MASTER_USER='slave_user', MASTER_PASSWORD='xxx', MASTER_LOG_FILE='mysql-bin.00xxxxxx', MASTER_LOG_POS=xxxxxxx, MASTER_SSL=1, MASTER_SSL_CA = '/etc/mysql/ca-cert.pem';
start slave;
show slave status \g
```
